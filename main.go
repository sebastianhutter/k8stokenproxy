package main

import (
	"context"
	"crypto/tls"
	"errors"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

type Client struct {
	port                  string
	dashboard             string
	namespace             string
	allow_insecure_tls    bool
	auth_group_header     string
	auth_group_name_admin string
	auth_group_name_ro    string
	auth_group_delimiter  string
	sa_admin              string
	sa_ro                 string
	sa_interval           time.Duration
	log_level             log.Level

	k8sconfig    *rest.Config
	k8sclientset *kubernetes.Clientset
}

var (
	c Client
)

func setVariable(e string, d string) string {
	// set environment variable with default value
	v := os.Getenv(e)
	if len(v) > 0 {
		return v
	} else {
		return d
	}
}

// retrieve kubernetes config object
// either running outside of cluster, with kubeconfig env var
// or running inside of cluster with serviceaccount
func retrieveConfiguration() (*rest.Config, error) {
	kubeconfig := os.Getenv("KUBECONFIG")
	if len(kubeconfig) > 0 {
		config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
		return config, err
	} else {
		config, err := rest.InClusterConfig()
		return config, err
	}
}

// initialize client set
func createClientSet(c *rest.Config) (*kubernetes.Clientset, error) {
	clientset, err := kubernetes.NewForConfig(c)
	return clientset, err
}

// retrieve service account, the token name and then get the secret of the token
func retrieveToken(n string, sa string, c *kubernetes.Clientset) (string, error) {
	service_account, err := c.CoreV1().ServiceAccounts(n).Get(context.TODO(), sa, metav1.GetOptions{})
	if err != nil {
		return "", err
	}

	// dont know how service accounts behave
	// lets assume we only ever have a single secret defined
	// in the service account and the secret is the sa token
	if len(service_account.Secrets) != 1 {
		return "", errors.New("Invalid secret count")
	}

	secret, err := c.CoreV1().Secrets(n).Get(context.TODO(), service_account.Secrets[0].Name, metav1.GetOptions{})
	if err != nil {
		return "", err
	}
	// When reading a secret, only Data contains any data, StringData is empty
	for key, value := range secret.Data {
		if key == "token" {
			// key is string, value is []byte
			return string(value), nil
		}
	}
	return "", errors.New("Unable to retrieve secret")

}

func init() {
	// load .env var
	_ = godotenv.Load(".env")

	// configure proxy service
	c.port = setVariable("PORT", "0.0.0.0:1234")
	c.dashboard = setVariable("DASHBOARD", "https://kubernetes-dashboard")
	c.namespace = setVariable("NAMESPACE", "default")
	c.sa_admin = setVariable("SERVICE_ACCOUNT_ADMIN", "kubernetes-dashboard-admin-user")
	c.sa_ro = setVariable("SERVICE_ACCOUNT_READONLY", "kubernetes-dashboard-readonly-user")
	duration, _ := strconv.Atoi(setVariable("SERVICE_ACCOUNT_INTERVAL", "60"))
	c.sa_interval = time.Duration(duration)
	c.auth_group_delimiter = setVariable("AUTH_GROUP_DELIMITER", ",")
	c.auth_group_header = setVariable("AUTH_GROUP_HEADER", "X-Auth-Request-Groups")
	c.auth_group_name_admin = setVariable("AUTH_GROUP_NAME_ADMIN", "kubernetes")
	c.auth_group_name_ro = setVariable("AUTH_GROUP_NAME_RO", "kubernetes-readonly")
	c.allow_insecure_tls, _ = strconv.ParseBool(setVariable("ALLOW_INSECURE_TLS", "true"))
	c.log_level, _ = log.ParseLevel(setVariable("LOG_LEVEL", "info"))

	// setup loglevel
	log.SetLevel(c.log_level)

	// setup kubernetes connection
	var err error
	// initialize configuration
	c.k8sconfig, err = retrieveConfiguration()
	if err != nil {
		panic(err.Error())
	}
	// setup client set
	c.k8sclientset, err = createClientSet(c.k8sconfig)
}

func main() {

	// start subroutines to retrieve the admin token
	c1 := make(chan string, 1)
	c2 := make(chan string, 1)

	go func() {
		for {
			log.Debug("Retrieve admin token")
			admin_token, err := retrieveToken(c.namespace, c.sa_admin, c.k8sclientset)
			if err != nil {
				log.Warnf("Unable to retrieve bearer token for service account %s", c.sa_admin)
				log.Warn(err)
			} else {
				c1 <- admin_token
			}
			time.Sleep(c.sa_interval * time.Second)
		}
	}()

	go func() {
		for {
			log.Debug("Retrieve readonly token")
			readonly_token, err := retrieveToken(c.namespace, c.sa_ro, c.k8sclientset)
			if err != nil {
				log.Warnf("Unable to retrieve bearer token for service account %s", c.sa_ro)
				log.Warn(err)
			}
			c2 <- readonly_token
			time.Sleep(c.sa_interval * time.Second)
		}
	}()

	// setup http reverse proxy
	var admin_token string
	var readonly_token string
	origin, _ := url.Parse(c.dashboard)
	director := func(req *http.Request) {

		var token string
		// retrieve tokens from channel
		select {
		case t := <-c1:
			admin_token = t
		default:
		}
		select {
		case t := <-c2:
			readonly_token = t
		default:
		}

		// check if we can find the request header containing the authorization groups
		for name, values := range req.Header {
			// Loop over all values for the name.
			if name == c.auth_group_header {
				if len(admin_token) > 0 {
					// if the header is found check if the admin group exists
					for _, v := range strings.Split(values[0], c.auth_group_delimiter) {
						if v == c.auth_group_name_admin {
							log.Debug("Found admin group in request header, set admin token for dashboard")
							token = admin_token
						}
					}
				}
				// if no admin token was found loop trough the group header again and check for the readonly grouop
				if len(readonly_token) > 0 && len(token) == 0 {
					for _, v := range strings.Split(values[0], c.auth_group_delimiter) {
						if v == c.auth_group_name_ro {
							log.Debug("Found readonly group in request header, set readonly token for dashboard")
							token = readonly_token
						}
					}
				}
			}
		}

		req.Header.Add("X-Forwarded-Host", req.Host)
		req.Header.Add("X-Origin-Host", origin.Host)
		if len(token) > 0 {
			req.Header.Add("Authorization", "Bearer "+token)
		}
		req.URL.Scheme = origin.Scheme
		req.URL.Host = origin.Host
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: c.allow_insecure_tls},
	}
	proxy := &httputil.ReverseProxy{Director: director, Transport: tr}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		proxy.ServeHTTP(w, r)
	})

	log.Fatal(http.ListenAndServe(c.port, nil))

}
