# k8stokenproxy

Inject service account bearer tokens into requests to the kubernetes dashboard. The goal of this service is to allow users which authenticated with oauth2-proxy direct access to the kubernetes dashboard.
This is useful if the API server of the kubernetes cluster can't be configured to use OIDC for authentication itself.

## Prerequisites
1. kubernetes dashboard is running with flag `--enable-skip-login`
2. kubernetes dashboard access is protected with [oauth2-proxy](https://github.com/oauth2-proxy/oauth2-proxy)
3. nginx ingress notations in place to pass authentication headers to this service (annotation: `nginx.ingress.kubernetes.io/auth-response-headers: X-Auth-Request-Groups`)


## Configuration options

The app is configured with envrionment variables

```bash
# the port the app is listening on
PORT=0.0.0.0:1234
# the kubernetes dashboard url
DASHBOARD=https://kubernetes-dashboard
# the namespace which is used to retrieve the service account tokens from
NAMESPACE=default
# the name of the service account with administrative rights for k8s dashboard access
SERVICE_ACCOUNT_ADMIN=kubernetes-dashboard-admin-user
# the name of the service account with readonly rights
SERVICE_ACCOUNT_READONLY=kubernetes-dashboard-readonly-user
# the duration in seconds between service account token checks
SERVICE_ACCOUNT_INTERVAL=60
# the delimiter character to split the user assigned groups
AUTH_GROUP_DELIMITER=,
# the header name containing the users groups
AUTH_GROUP_HEADER=X-Auth-Request-Groups
# the name of the user group which should allow admin access
AUTH_GROUP_NAME_ADMIN=kubernetes
# the name of the user group which should allow readonly access
AUTH_GROUP_NAME_RO=kubernetes-readonly
# allow insecure tls from app to kubernetes dashboard (e.g. for local testing)
ALLOW_INSECURE_TLS=true
```

## rbac permissions
the token proxy serviceaccount needs read permissions for secrets ans service accounts
in the kuberntes dashboard namespace
```yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: k8s-dashboard-tokenproxy
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: k8s-dashboard-tokenproxy
rules:
  - apiGroups: [""]
    resources: ["serviceaccounts"]
    verbs: ["get"]
  - apiGroups: [ "" ]
    resources: [ "secrets" ]
    verbs: [ "get" ]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: k8s-dashboard-tokenproxy
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: k8s-dashboard-tokenproxy
subjects:
  - kind: ServiceAccount
    name: k8s-dashboard-tokenproxy
```
