module gitlab.com/sebastianhutter/k8stokenproxy

go 1.16

require (
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.8.1
	k8s.io/apimachinery v0.21.0
	k8s.io/client-go v0.21.0
)
