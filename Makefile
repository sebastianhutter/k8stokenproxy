PROJECT_NAME := "k8stokenproxy"
DOCKER_IMAGE := sebastianhutter/$(PROJECTNAME)

.PHONY: all dep build clean test

all: build

test:
	@go test -short

build:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags="-w -s" -o $(PROJECT_NAME)

docker: build
	docker build -t sebastianhutter/$(PROJECT_NAME) .
	docker push sebastianhutter/$(PROJECT_NAME)
